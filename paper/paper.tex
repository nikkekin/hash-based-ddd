
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}

\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{color}

\usepackage{url}


\usepackage{amsmath,amsthm,amssymb}
\usepackage{framed}


\newcommand*{\defeq}{\mathrel{\vcenter{\baselineskip0.5ex \lineskiplimit0pt
                     \hbox{\scriptsize.}\hbox{\scriptsize.}}}%
                     =}
\DeclareMathOperator*{\argmin}{arg\,min}
\newcommand{\BigO}[1]{\ensuremath{\operatorname{O}\left(#1\right)}}
\def\!#1{\ensuremath{\mathbf{#1}}}
\def\*#1{\ensuremath{\mathcal{#1}}}

\newcommand{\family}{\ensuremath{\*F^{\!S}_l}}
\newcommand{\locality}{\ensuremath{\ell^S_M}}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Hashing-based Delayed Duplicate Detection for Bayesian Network Structure Learning}

% a short form should be given in case it is too long for the running head
\titlerunning{Hashing-based DDD for Bayesian Network Structure Learning}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Niklas Jahnsson and Brandon Malone}
%
\authorrunning{Hashing-based DDD for Bayesian Network Struture Learning}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{University of Helsinki and Max Planck Institute for the Biology of Ageing}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{Hashing-based Delayed Duplicate Detection for Bayesian Network Structure Learning}
\tocauthor{Niklas Jahnsson and Brandon Malone}
\maketitle


\begin{abstract}
In this work, we address the well-known score-based Bayesian network structure learning problem (BNSL). Breadth-first branch and bound (BFBnB) has been shown to be an effective approach for solving this algorithm. Delayed duplicate detection (DDD) is an important component of the BFBnB algorithm. Previously, an external sorting-based technique, with complexity O(n log n), was used for DDD. In this work, we propose a hashing-based technique, with comlexity O(n), for DDD. Empirical results suggest hashing-based DDD is competitive with the state of the art. Additionally, the proposed hash function could also be used to, for example, efficiently parallelize BFBnB.
\keywords{Bayesian networks, state space search, delayed duplicate detection}
\end{abstract}

\section{Introduction}

\section{Background}

\subsection{Bayesian networks}

A Bayesian network~\cite{Pearl1988} is a compact representation of a joint probability distribution over the random variables $\!V = \{X_1, \dots, X_n\}$.
It consists of a directed acycic graph (DAG) in which each vertex corresponds to one of the random variables;
the edges in the graph indicate conditional independencies among the variables.
Additionally, each variable $X_i$ has an associated probability distribution, conditioned on its parents in the DAG, $PA_i$.
The joint probability distribution is as follows.
\begin{equation}
P(\!V) = \prod_i^n P(X_i | PA_i)
\end{equation}

Given a dataset $\*D = \{D_1, \dots D_N\}$, where each $D_i$ is a complete instantiation of $\!V$, the goal of structure learning is to find a Bayesian network $\*N$ which best fits $\*D$.
The fit of $\*N$ to $\*D$ is quantified by a scoring function $s$.
Many scoring functions have been proposed in the literature, including Bayesian scores~\cite{Cooper1992,Heckerman1995}, MDL-based scores~\cite{Suzuki1993,Silander2008}, and independence~\cite{Campos2000}.
The scoring functions can typically be interpretted as penalized log-likelihood functions.
All commonly used scoring functions are \emph{decomposable}~\cite{Heckerman1995};
that is, they decompose into a sum of \emph{local scores} for each variable, its parents, and the data,
\begin{equation}
s(\*N; \*D) = \sum_i^n s_i(PA_i; \*D) \text{,}
\end{equation}
where $s_i(PA_i)$ gives the score of $X_i$ using $PA_i$ as its parents and is non-negative.
We omit $\*D$ when it is clear from context.

A variety of pruning rules~\cite{Suzuki1993,Tian2000,Teyssier2005,Campos2008} can be applied to demonstrate that some parents sets are never optimal for some variables.
Additionally, in practice, large parent sets are often pruned \textit{a priori}~\cite{Malone2015}.
We refer to parent sets remaining after such pruning as \emph{candidate parent sets} and denote all candidate parent sets of $X_i$ as $\*P_i$.

The \emph{Bayesian network structure learning} problem (BNSL) is then defined as follows\footnote{The problem can also be defined as a maximization using non-positive local scores.}.

\begin{framed}
\begin{minipage}{\columnwidth}
\paragraph{The BNSL Problem}

\begin{description}
\item[\textsc{Input:}]
A set $\!V = \{X_1,\ldots,X_n\}$ of variables and a local score $s_i(PA_i)$ for each $PA_i \in \*P_i$ for each $X_i$.
\item[\textsc{Task:}]
Find a DAG $N^*$ such that
\begin{align*}
\label{eq:bnsl-objective}
N^* \in\argmin_{N} \sum_{i=1}^n s_i(PA_i),
\end{align*}
where $PA_i$ is the parent set of $X_i$ in $N$ and $PA_i \in \*P_i$.
\end{description}
\end{minipage}
\end{framed}


\subsection{State space search for BNSL}
\label{sec:state-space-search}

Many techniques have been used to solve BNSL.
State space search algorithms are one state-of-the-art ~\cite{Yuan2013,Malone2014}.
In this approach, a state space is formulated based on the well-known dynamic programming recurrence.
Each node in the space corresponds to an optimal network over a subset of the variables.

Expanding a node amounts to adding each remaining variable of its current subnetwork.
Thus, a path in the implicit graph from the start node, with no variables, to the goal node, with all the variables, corresponds to a total ordering over the variables;
consequently, this graph is called the \emph{order graph}.
The cost of the path exactly gives the cost of the corresponding network.
Thus, BNSL can be solved by finding the shortest path.

Several algorithms, such as A*~\cite{Yuan2013} and depth-first search~\cite{Malone2013a} have been used to solve this problem.
Breadth-first branch and bound~\cite{Malone2011,Fan2014} (BFBnB) has been shown to be an effective search strategy.
In BFBnB, nodes are expanded in a layer-wise fashion.
Due to the known structure of the order graph, the cost of the shortest path is found after expanding the $n^{th}$ layer.
The optimal network can be reconstructed using standard back-pointer techniques~\cite{Russell2003} or more memory-efficient recursive strategies~\cite{Zhou2003a}.

\textcolor{blue}{Pruning here}

One of the main operators of BFBnB is \emph{duplicate detection} (DD).
In the context of BNSL, duplicates are subnetworks over the same set of variables.
Due to Equation~\ref{eqn:XXX}, DD entails selecting the subnetwork with the minimum score.

A typical approach to DD is to use an in-memory hash table to detect nodes for the same subnetwork.
The hash table retains the best copy of a node found so far.
Since the hash table is used to detect duplicates as soon as nodes are generated, we refer to this strategy as \emph{immediate duplicate detection} (IDD).
For BNSL, the in-memory hash table would need to store, in the worst case, all of the nodes in one layer.
The largest layer of the order graph contains \BigO{\binom{n}{\frac{n}{2}}}, which is exponential in the number of variables.

Consequently, previous work used \emph{delayed duplicate detection} (DDD).
The essence of DDD is to use external memory, such as hard disk, to store nodes.
Then, after expanding all of the nodes in one layer, efficient disk access technqiues are used to remove duplicates from disk.

\textcolor{blue}{something about the regular structure of the order graph}

\subsection{Hybrid duplicate detection}

The previous BFBnB algorithm for BNSL~\cite{Malone2011} used a sorting-based \emph{hybrid duplicate detection} (HDD) technique.
In this approach, IDD is performed until the hash table reaches a user-specified size;
then, the nodes are sorted and written to an external memory file.
After expanding all of the nodes in one layer, DDD is performed by using an external-memory merge sort operation~\cite{Korf2004}.
The space complexity of sorting-based DDD is dominated by sorting, which is dependent on the number of nodes written to disk, $m$, and requires \BigO{m \log m} space.

\textcolor{blue}{somewhere, a sentence like ``previous work has shown the space is the main limiting factor for this approach, so we focus our analysis on space''}.

\textcolor{red}{This needs work}
While difficult to analyze, an empirically important aspect of HDD is \emph{locality}.
In this context, locality refers to duplicates detected with IDD.
Specifically, we define the locality of search strategy $S$ as follows.
\begin{equation}
\locality = \frac{n_u}{n_w} \text{,}
\end{equation}
where $S$ is the HDD strategy, $M$ is the memory bound, $n_u$ is the number of unqiue nodes in the search space and $n_w$ is the number of nodes written to disk.
So, algorithms with a high locality remove most duplicates with IDD.
In particular, if IDD removes all duplicates, then $\locality = 1$.

Obviously, increasing locality reduces the time spent on expensive external memory read and write operations.
\textcolor{red}{this statement is too vague}
Clearly, larger $m$ allows for more effective IDD and improves locality.

As previously mentioned, sorting the hash table requires \BigO{m \log m} space.
Thus, in order to ensure sorting does not exceed the memory bound $M$, the size of the hash table is restricted to $\frac{M}{\log m}$.
So, the sorting operation impairs locality because the hash table cannot use all of the available memory.
\textcolor{red}{Some analysis of random expecations can be used here to make this a real argument.}

\textcolor{blue}{This is somewhat assuming the load factor of the hash table is the same for sorting- and hashing-based DDD.
It is probably also assuming that the load factor is close 1.}

\section{Hashing-based DDD for BNSL}
\label{sec:hashing-based-ddd}

In this work, we propose to use hashing-based DDD~\cite{Korf2008}.
In this approach to DDD, rather than writing nodes to multiple sorted files, a hash function is used to distribute nodes to files.
So, rather than creating a new, sorted file when writing nodes to disk, nodes are appended to multiple existing files.
The hash function must ensure all duplicates are written to the same file.
After expanding all of the nodes in each layer, each external file is sequentially read back into an in-memory hash table to identify and remove duplicates.
The space complexity of hashing-based DDD is \BigO{m}.
Since hashing-based DDD avoids the $\log m$ overhead of sorting, it allows the use of the entire $M$ space for the hash table;
\textcolor{blue}{consequently, hashing-based DDD increases locality compared to sorting-based DDD for random node generation strategies.}

\subsection{Dividing nodes into buckets}
\label{sec:dividing-nodes}

\textcolor{blue}{If we have space, it would be nice to include a table of symbols.}

The basic idea for dividing order graph nodes into hash buckets is based on the use of subcombinations, somewhat similar to the approach used by Tamada \textit{et al.} for parallelization~\cite{Tamada2011}.
We refer to $\!S \subset \!U$ as a \emph{subcombination} of \!U.
Furthermore, when \!S contains the lexicographically first $k$ elements of \!U, we refer to \!S as a \emph{first-$k$ subcombination} of \!U.
Additionally, when \!S is a first-$k$ subcombination of \!U, we refer to \!U as an \emph{extension} of \!S.
We denote all extensions of \!S of size $l$ as \family (the ``family'' of \!S).
In general, \family contains \BigO{C(n-k, l-k)} elements, where $C(n-k, l-k)$ is the binomial coefficient.

The families form the basis of our approach for dividing order graph nodes into buckets.
We assign each node to the lexicographically first family to which it belongs.
For example, $\!U = \{X_1, X_2, X_3\}$ would belong to $\*F^{\{X_1, X_2\}}_3$ rather than $\*F^{\{X_1, X_3\}}_3$.
In principle, each family will correspond to one hash bucket (see also Section~\ref{sec:distributing-buckets}).
We use ``buckets'' and ``families'' interchangeably.

\subsection{Optimizing bucket size}
\label{sec:optimizing-bucket-size}

For a given value of $k$ and $n$, we have $C(n,k)$ buckets.
In order to minimize hashing-related overhead, we aim to minimize the number of buckets.
As mentioned in Section~\ref{sec:dividing-nodes}, at layer $l$ with $k$ buckets, the largest bucket contains $C(n-k, l-k)$ nodes.

As a restriction on the memory use of the algorithm, we assume at most $m$ nodes fit in memory at once.
Thus, we select $k$ such that the largest bucket contains no more than $m$ nodes.
Consequently, at each layer of the order graph, we select $k$ by solving
\begin{equation}
    \label{eqn:min-k}
    \argmin_k C(n-k, l-k) \le m\text{,}
\end{equation}
where $n$, $l$ and $m$ are fixed and $0<k<l$.

\subsection{Distributing buckets to files}
\label{sec:distributing-buckets}

After choosing $k$ by solving Equation~\ref{eqn:min-k}, the \emph{maximum} number of nodes in any bucket is fixed at $C(n-k, l-k)$.
However, as mentioned in Section~\ref{sec:dividing-nodes}, nodes are distribution to the lexicographically first bucket to which they could belong.
For example, $\*F^{\{X_1, X_2\}}_3$ ``steals'' $\{X_1, X_2, X_3\}$ from $\*F^{\{X_1, X_3\}}_3$.
Therefore, $\*F^{\{X_1, X_3\}}_3$ will contain one node less than given by the bound.

In fact, because nodes are assigned to the lexicographically first bucket possible, \family is assigned only nodes which follow \!S lexicographically.
This is, if we take $X_i$ as the lexicographically last element of \!S, then all extensions assigned to \family use only variables $X_j$ such that $j > i$.
So, due to stealing, \family, with $X_i$ as the lexicographically last element, is assigned $C^\prime(n-i, l-k)$ nodes, where $C^\prime(n-i,l-k)$ is defined to be $0$ when $0 < n-i < l-k$ and binomial coefficient otherwise.  

Furthermore, we can calculate the number of buckets ending with $X_i$.
Since the lexicographically last element of the subcombination for the bucket is $X_i$, the other $k-1$ elements must come from the $i-1$ elements which precede $X_i$.
Thus, $C(i-1, k-1)$ buckets end with $X_i$. \textcolor{red}{What do we use this information for?}

\subsection{Packing algorithms}
For a variety of reasons, such as operating system constraints on open file handles and latencies associated with randomly accessing hard disk, we aim to minimize the number of files used for storing the buckets.
Thus, we assign many buckets to a single file.
The constraint on assigning buckets is that the sum of the sizes of buckets assigned to a single file cannot exceed $M$.

Based on the previous observations, for fixed values of $k$ and $n$, the number of buckets and all of their sizes is known.
Thus, we can solve a bin packing problem to assign buckets to files in order to minimize the number of required files.
Bin packing is known to be NP-hard~\cite{Garey1979}; 
however, a variety of efficient approximation algorithms are available for this problem~\cite{Johnson1973,Korf2002}.

In Section~\ref{sec:experiments}, we use two different bin packing algorithms for assigning buckets to files.
First, we use an optimal packing which guarantees to minimize the number of files.
Second, we use the first-fit decreasing (FFD) approximation algorithm~\cite{Johnson1973}, which is known to be an $\frac{11}{9}$-approximation.

In practice, for HDD search algorithms, all of the nodes in one bucket are expanded before moving to the next bucket.
\textcolor{red}{This statement can be clarified more.}
Consequently, a second goal of assigning buckets to files for HDD is to maximize locality by assigning lexicographically-similar subcombinations to the same bucket.
To the best of our knowledge, no algorithms solve this problem.

We do not explicitly incorporate locality into packing because that would lead to a multi-objective optimization problem.
\textcolor{red}{This can be clarified more.}
Nevertheless, in practice, FFD assigns buckets in lexicographic order.
Thus, FFD implicitly uses a packing strategy that leads to high locality.

\textcolor{red}{It may be possible to use the work on super-combinations by Tamada et al., to analytically calculate the amount of locality we get when assigning buckets to files.}



\section{Experiments}
\label{sec:experiments}

As discussed in Section~\ref{sec:hashing-based-ddd}, locality is an important factor in the performance of hybrid duplicate detection.
Thus, we designed a set of experiments to better understand how hashing-based HDD affects locality.
The packing strategy used to combine buckets into files (Section~\ref{sec:distributing-buckets}) affects locality, so we also investigated it in more detail.
\textcolor{red}{This does not actually match up with the second hypothesis right now.
I believe if we can come up with a second packing strategy, that will improve the paper.}
In particular, we tested the following three hypotheses.

\begin{enumerate}
    \item Given a fixed amount of memory $M$, hashing-based HDD has a higher locality than sorting-based HDD.
    \item The greedy packing algorithm in Section~\ref{sec:distributing-buckets} is close to optimal in terms of minimizing external memory files.
    \item For a fixed packing strategy, locality and $M$ are strongly positively correlated.
\end{enumerate}

\subsection{Datasets and environment}

All of the experiments were run on dual quad-core Intel Xeon E5540 processors with 32 GB of RAM.
Hard disk space was limited to 16 GB.

As described in Section~\ref{sec:state-space-search}, pruning is very important for the performance of BFBnB;
however, pruning is dependent both on the quality of the bound and the quality of the heuristic.
These are both data-dependent and difficult to analytically characterize.
Consequently, in order to remove this confounding factor from our analysis, we do not using pruning.

We use datasets from a previous study~\cite{Malone2014}.
Since we also do not use pruning, locality is the same for all datasets with the same number of variables for a fixed packing strategy.
Thus, we only use one dataset for each number of variables.

\subsection{Locality comparison}

\subsection{Packing efficiency}

\subsection{Locality and memory correlation}

\section{Discussion}

\bibliography{library}
\bibliographystyle{apalike}

\end{document}
